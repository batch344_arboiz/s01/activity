import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner averageGradeInput = new Scanner(System.in);

        System.out.println("First Name: ");
        String firstName = averageGradeInput.nextLine();
        System.out.println("Last Name: ");
        String lastName = averageGradeInput.nextLine();
        System.out.println("First Subject Grade: ");
        double firstGrade = averageGradeInput.nextDouble();
        System.out.println("Second Subject Grade: ");
        double secondGrade = averageGradeInput.nextDouble();
        System.out.println("Third Subject Grade: ");
        double thirdGrade = averageGradeInput.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName );
        System.out.println("Your grade average is: " + Math.floor((firstGrade+secondGrade+thirdGrade)/3));
    }
}